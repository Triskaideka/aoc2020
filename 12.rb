=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 12
https://adventofcode.com/2020/day/12
=end

#instructions = IO.readlines("12_input_example.txt", chomp: true)
instructions = IO.readlines("12_input.txt", chomp: true)

# Parse the instructions.
instructions.map!{ |inst|

  m = inst.match(/^(\w)([0-9]+)$/)

  { action: m[1], value: m[2].to_i }

}



# Part 1
# "Figure out where the navigation instructions lead. What is the Manhattan
# distance between that location and the ship's starting position?"

# Class for easily turning and moving the ship.
class Ship

  attr_reader :facing, :latitude, :longitude



  # The problem statement doesn't really describe the positions as "latitude"
  # and "longitude", but it's helpful to give them memorable names.
  def initialize (fac = "E", lat = 0, long = 0)
    @facing = fac
    @latitude = lat
    @longitude = long

    @directions = ["N", "E", "S", "W"]
  end



  def follow_instruction (action, value)

    #puts "executing instruction: #{action}#{value}"

    case action
    when "L"
      self.turn_left(value)
    when "R"
      self.turn_right(value)
    else
      self.move(action, value)
    end

  end



  def move (direction, distance)

    if direction == "F" then
      direction = @facing
    end

    if direction == "N" then
      @latitude += distance
    elsif direction == "E" then
      @longitude += distance
    elsif direction == "S" then
      @latitude -= distance
    elsif direction == "W" then
      @longitude -= distance
    else
      raise "\"#{direction}\" is not a direction in which the ship can move."
    end

  end



  def turn (rotation, degrees)

    index = @directions.find_index(@facing)

    while (degrees > 0) do
      index += rotation

      # Wrap around as necessary:
      if index < 0 then index = @directions.length - 1 end
      if index >= @directions.length then index = 0 end

      @facing = @directions[index]
      degrees -= 90  # we only turn at right angles
    end

  end



  def turn_left (degrees)
    self.turn(-1, degrees)
  end



  def turn_right (degrees)
    self.turn(1, degrees)
  end

end  # class Ship



# The ship's "Manhattan distance" is the "sum of the absolute values of its
# east/west position and its north/south position".
def manhattan (a, b)
  return a.abs + b.abs
end



# "The ship starts by facing east."
ferry = Ship.new("E")



# Execute the instructions.
instructions.each do |inst|
  ferry.follow_instruction(inst[:action], inst[:value])
end



#puts "The ferry is at lat. #{ferry.latitude}, long. #{ferry.longitude}"
puts manhattan(ferry.latitude, ferry.longitude)



# Part 2
# "Figure out where the navigation instructions actually lead. What is the
# Manhattan distance between that location and the ship's starting position?"

# Open the Ship class back up to make some revisions.
# There's a fair amount of copy-and-paste here.  Of course in a real-life
# situation, I would have just deleted the code from part 1 that we now know
# to be incorrect.
class Ship

  def initialize (fac = "E", lat = 0, long = 0, wlat = 1, wlong = 10)
    @facing = fac
    @latitude = lat
    @longitude = long
    @waypoint = { latitude: wlat, longitude: wlong }

    @directions = ["N", "E", "S", "W"]
  end



  def follow_instruction (action, value)

    #puts "executing instruction: #{action}#{value}"

    case action
    when "F"
      value.times{ self.move_to_waypoint() }
    when "L", "R"
      self.rotate_waypoint(action, value)
    else
      self.move_waypoint(action, value)
    end

    #puts "The ship is at long. #{@longitude}, lat. #{@latitude} " +
    #     "and the waypoint is at long. #{@waypoint[:longitude]}, lat. #{@waypoint[:latitude]} relative to that"

  end



  # Don't confuse "move_waypoint" and "move_to_waypoint".
  def move_waypoint(direction, distance)

    if direction == "N" then
      @waypoint[:latitude] += distance
    elsif direction == "E" then
      @waypoint[:longitude] += distance
    elsif direction == "S" then
      @waypoint[:latitude] -= distance
    elsif direction == "W" then
      @waypoint[:longitude] -= distance
    else
      raise "\"#{direction}\" is not a recognized direction."
    end

  end



  def move_to_waypoint
    @latitude += @waypoint[:latitude]
    @longitude += @waypoint[:longitude]
  end



  # "wise" as in "clockwise" or "counterclockwise"
  def rotate_waypoint (wise, degrees)

    while (degrees > 0) do

      prev_lat = @waypoint[:latitude]
      prev_long = @waypoint[:longitude]

      # Note that the difference between these two branches is the addition/subtraction operators.
      # clockwise
      if wise == "R" then
        @waypoint[:latitude] = 0 - prev_long
        @waypoint[:longitude] = 0 + prev_lat

      # counterclockwise
      else
        @waypoint[:latitude] = 0 + prev_long
        @waypoint[:longitude] = 0 - prev_lat

      end

      degrees -= 90  # we only turn at right angles

    end

  end  # method rotate_waypoint

end  # class Ship



ferry = Ship.new("E", 0, 0, 1, 10)

instructions.each do |inst|
  ferry.follow_instruction(inst[:action], inst[:value])
end

#puts "The ferry is at lat. #{ferry.latitude}, long. #{ferry.longitude}"
puts manhattan(ferry.latitude, ferry.longitude)
