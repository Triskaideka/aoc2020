=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 8
https://adventofcode.com/2020/day/8
=end

boot_code = IO.readlines("08_input.txt", chomp: true)

# Part 1
# "Immediately before any instruction is executed a second time,
# what value is in the accumulator?"

# Accepts: an array of instructions
# Returns: a Boolean indicating exit status, and an integer with the last value of the accumulator
def run_program(program)

  accumulator = 0
  instruction_execution_count = Hash.new(0)
  ipointer = 0

  while ipointer < program.length do

    # Prevent infinite loops.
    if instruction_execution_count[ipointer] > 0 then
      #puts "Halting because we've arrived at instruction \##{ipointer} a second time."
      break
    end

    # Record having executed this instruction.
    instruction_execution_count[ipointer] += 1

    # Parse the instruction.
    command, argument = program[ipointer].split(/\s+/)
    argument = argument.to_i  # This will take the argument's sign into account.

    # Execute the instruction.
    case command
    when "acc" then
      accumulator += argument
    when "jmp" then
      ipointer += argument
    when "nop" then
      # do nothing
    end

    # Increase the instruction pointer by one, unless the command specifically
    # said to modify it in a different way.
    if ! command.eql?("jmp") then
      ipointer += 1
    end

    # Check if the pointer's gone out of bounds.
    # Note that we don't use >= since the pointer being exactly equal to
    # program.length is the desired end state (see part 2).
    if ipointer < 0 || ipointer > program.length then
      die("The instruction pointer has been set to an out-of-bounds value (#{ipointer}).")
    end

  end

  # Figure out whether this program terminated normally.
  exit_status = false
  if ipointer == program.length then
    exit_status = true
  end

  # Return two values as indicated in the comments preceding this function.
  return exit_status, accumulator

end

status, result = run_program(boot_code)
#puts status
puts result



# Part 2
# "Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp).
# What is the value of the accumulator after the program terminates?"
# A program "terminates normally" when it attempts "to execute an instruction immediately after the
# last instruction in the file."

(0...boot_code.length).each do |instruction_to_change|

=begin
I tried to do:
    modified_program = boot_code.clone
but that didn't achieve the desired purpose.  Modifying the clone array still
affected the original array.  The answer at https://stackoverflow.com/a/31455402
provided the code I needed, and the explanation.  But... ugh.  This is one of my
least favorite things about Ruby.  It shouldn't be so hard to get a separate
copy of a variable that you can experiment with while leaving the original
intact.  I would say it's probably the most common reason why I ever assign one
variable to another.

The .clone method worked when I used it for the Day 6 problem.  Or maybe it's
not really doing what I want, even there, and I just got lucky that it didn't
adversely affect the program?
=end
  modified_program = Marshal.load(Marshal.dump(boot_code))

  # If this instruction is a nop change it to a jmp, and vice versa.
  # If it's neither, move on to the next instruction.
  if modified_program[instruction_to_change].gsub!(/^nop/, 'jmp').nil? then
    if modified_program[instruction_to_change].gsub!(/^jmp/, 'nop').nil? then
      next
    end
  end

  # Run the program with the one changed instruction.
  status, result = run_program(modified_program)

  # If the exit status indicates success, print the result. And we can quit,
  # since the problem statement says there's only one possible solution.
  if status then
    puts result
    break
  end

end
