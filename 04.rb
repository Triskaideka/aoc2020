=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 4
https://adventofcode.com/2020/day/4
=end

batch = IO.readlines("04_input.txt", chomp: true)
passports = [{}]

# Part 1
# "Count the number of valid passports - those that have all required fields.
# Treat cid as optional. In your batch file, how many passports are valid?"

ppid = 0
required_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

batch.each do |line|

  # If there's nothing on this line, it's a break between passports.
  # Multiple empty lines will increase the passport counter multiple times.
  # That's okay; we'll remove the blank ones later.
  if line.empty? then
    ppid += 1
    passports[ppid] = {}
    next
  end

  line.split(' ').each do |field|

    # split the field on the first semicolon
    fld_name, fld_value = field.split(':', 2)

    # put the field into this passport hash
    passports[ppid][fld_name] = fld_value

  end

end

# Throw away any passports that have NO fields.
passports.reject! { |passport| passport.empty? }

# Inspect each passport for validity
passports.each do |passport|

  passport["valid"] = true

  required_fields.each do |rfield|
    if ! passport.has_key?(rfield) then
      passport["valid"] = false
    end
  end

end

puts passports.select{ |passport| passport["valid"] }.length



# Part 2
# "Your job is to count the passports where all required fields
# are both present and valid according to the above rules."
#
# Having already printed the answer to part 1, we'll now re-purpose the "valid" key for part 2.
# Any passport that was already invalid cannot be rescued by good formatting, but some passports
# that previously looked okay are going to be invalidated by the stricter requirements.

passports.each do |passport|

  # Skip any passport that was already determined to be invalid.
  # Now we don't have to bother testing whether each field is present before we test its contents.
  next if ! passport["valid"]



  # birth year
  if ! passport["byr"].match?(/^[0-9]{4}$/) ||
     passport["byr"].to_i < 1920 ||
     passport["byr"].to_i > 2002
  then
    passport["valid"] = false
  end



  # issue year
  if ! passport["iyr"].match?(/^[0-9]{4}$/) ||
     passport["iyr"].to_i < 2010 ||
     passport["iyr"].to_i > 2020
  then
    passport["valid"] = false
  end



  # expiration year
  if ! passport["eyr"].match?(/^[0-9]{4}$/) ||
     passport["eyr"].to_i < 2020 ||
     passport["eyr"].to_i > 2030
  then
    passport["valid"] = false
  end



  # height
  if m = passport["hgt"].match(/^([0-9]{2,3})(cm|in)$/) then

    # centimeters
    if (m[2] == "cm") then

      if m[1].to_i < 150 || m[1].to_i > 193 then
        passport["valid"] = false
      end

    # inches
    else

      if m[1].to_i < 59 || m[1].to_i > 76 then
        passport["valid"] = false
      end

    end

  # if the height field didn't match its regular expression at all, then it's invalid
  else
    passport["valid"] = false
  end



  # hair color
  if ! passport["hcl"].match?(/^#[0-9a-f]{6}$/) then
    passport["valid"] = false
  end



  # eye color
  if ! ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].include?(passport["ecl"]) then
    passport["valid"] = false
  end



  # passport id -- do not confuse with this program's internal passport ID, called "ppid"
  if ! passport["pid"].match?(/^[0-9]{9}$/) then
    passport["valid"] = false
  end

end

puts passports.select{ |passport| passport["valid"] }.length
