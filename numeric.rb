# This is the best method for determining whether a string is a number in Ruby, according to:
# https://mentalized.net/journal/2011/04/14/ruby-how-to-check-if-a-string-is-numeric/
class String
  def numeric?
    Float(self) != nil rescue false
  end
end
