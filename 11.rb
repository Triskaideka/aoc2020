=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 11
https://adventofcode.com/2020/day/11

This program takes a few seconds to run, but it does produce the correct results.
=end

#layout = IO.readlines("11_input_example.txt", chomp: true)
layout = IO.readlines("11_input.txt", chomp: true)

# Seats are represented as strings in the puzzle, but let's make a sub-class
# with some convenience methods to help make it clearer what we're doing
# throughout this program.
class Seat < String

  def is_empty_seat?
    return self === 'L'
  end

  def is_occupied_seat?
    return self === '#'
  end

  # You can't assign to self using '=', but you can change it using '.replace'.
  def empty_seat
    if self.is_occupied_seat? then self.replace('L') end
  end

  def occupy_seat
    if self.is_empty_seat? then self.replace('#') end
  end

end  # class Seat

# Now turn the layout into a two-dimensional Array of Seats.
layout.map!{ |row| row.chars.map!{ |c| Seat.new(c) } }



# Part 1
# "Simulate your seating area by applying the seating rules repeatedly until
# no seats change state. How many seats end up occupied?"

# Given a grid and a position within it,
# return the number of occupied seats in the eight surrounding positions.
#
# This method works by creating a smaller grid, consisting only of the spaces
# adjacent to the given position, and then passing that grid off to the
# count_occupied_seats method, which already knows how to do the work we want
# done.
def occupied_seats_adjacent(grid, x, y)

  # This code is functional, but it's so ugly.
  #
  # Ruby's array-accessing features are so slick, I feel like there must be a
  # better way to do this that I just can't figure out.
  #
  # What I really want to do is specify a range of indexes, and get back an
  # array that consists of all the elements in that range *that exist*.
  #
  # Array#fetch lets you specify a default if the index doesn't exist, but it
  # only takes an index, not a range.
  #
  # There's also the problem that if x or y are 0, I'll be sending -1 as part
  # of the range, and that *is* a valid way to access an array -- I'm even using
  # it in this code -- but it does something other than what I want to do here.
  test_array = [[]]

  if x > 0 then
    if y > 0 then test_array[-1] << grid[x-1][y-1] end
    test_array[-1] << grid[x-1][y]
    if y < (grid[x].length - 1) then test_array[-1] << grid[x-1][y+1] end
  end

  test_array.push([])

  if y > 0 then test_array[-1] << grid[x][y-1] end
  if y < (grid[x].length - 1) then test_array[-1] << grid[x][y+1] end

  test_array.push([])

  if x < (grid.length - 1) then
    if y > 0 then test_array[-1] << grid[x+1][y-1] end
    test_array[-1] << grid[x+1][y]
    if y < (grid[x].length - 1) then test_array[-1] << grid[x+1][y+1] end
  end

  return count_occupied_seats(test_array)

end  # method occupied_seats_adjacent



# Given a grid, return the number of occupied seats in it.
def count_occupied_seats(grid)

  occ = 0

  grid.each do |row|
    row.each do |seat|
      if seat.is_occupied_seat? then
        occ += 1
      end
    end
  end

  return occ

end  # method count_occupied_seats



current_layout = Marshal.load(Marshal.dump(layout))

loop do

  next_layout = Marshal.load(Marshal.dump(current_layout))

  current_layout.each_with_index do |row, i|
    row.each_with_index do |seat, j|

      if seat.is_empty_seat? &&
         occupied_seats_adjacent(current_layout, i, j) === 0
      then
        next_layout[i][j].occupy_seat

      elsif seat.is_occupied_seat? &&
            occupied_seats_adjacent(current_layout, i, j) >= 4
      then
        next_layout[i][j].empty_seat

      end

    end
  end

  # If the seating arrangement hasn't changed during this loop, then we're done.
  # This is the only way that the program can exit this loop.
  break if current_layout == next_layout

  # Otherwise, shift the seating charts, and go again.
  current_layout = Marshal.load(Marshal.dump(next_layout))

end  # do-loop

# # Debugging code:
# layout.each{ |row| puts row.join('') }
# print "\n----------------------------------------------------------------------------------------------------\n\n"
# current_layout.each{ |row| puts row.join('') }

puts count_occupied_seats(current_layout)



# Part 2
# "Given the new visibility method and the rule change for occupied seats
# becoming empty, once equilibrium is reached, how many seats end up occupied?"

# Given a grid and a position within it, return the
# number of occupied seats visible from it in each of the eight directions.
def occupied_seats_visible(grid, x, y)

  occ = 0

  (-1..1).each do |x_slope|
    (-1..1).each do |y_slope|
      if occupied_seat_visible_in_direction(grid, x, y, x_slope, y_slope) then
        occ += 1
      end
    end
  end

  return occ

end  # method occupied_seats_visible



# Given a grid, a position, and a direction, return a Boolean value
# indicating whether an occupied seat is visible when looking in that direction
# from that position.
def occupied_seat_visible_in_direction(grid, x, y, x_slope, y_slope)

  # A seat isn't visible from itself.
  return false if x_slope === 0 && y_slope === 0

  # Move to the first seat in the given direction.
  x += x_slope
  y += y_slope

  while x >= 0 &&
        y >= 0 &&
        x <= (grid.length - 1) &&
        y <= (grid[x].length - 1) do

    if grid[x][y].is_occupied_seat? then
      return true
    elsif grid[x][y].is_empty_seat? then
      return false
    end

    # Move to the next seat in the given direction.
    x += x_slope
    y += y_slope

  end

  # If you can look clear to the edge of the grid, then you can't see a seat.
  return false

end  # method occupied_seat_visible_in_direction



# The rest of part 2 is close to a copy-and-paste of part 1.
current_layout = Marshal.load(Marshal.dump(layout))

loop do

  next_layout = Marshal.load(Marshal.dump(current_layout))

  current_layout.each_with_index do |row, i|
    row.each_with_index do |seat, j|

      if seat.is_empty_seat? &&
         occupied_seats_visible(current_layout, i, j) === 0
      then
        next_layout[i][j].occupy_seat

      elsif seat.is_occupied_seat? &&
            occupied_seats_visible(current_layout, i, j) >= 5
      then
        next_layout[i][j].empty_seat

      end

    end
  end

  # If the seating arrangement hasn't changed during this loop, then we're done.
  # This is the only way that the program can exit this loop.
  break if current_layout == next_layout

  # Otherwise, shift the seating charts, and go again.
  current_layout = Marshal.load(Marshal.dump(next_layout))

  # # Debugging code:
  # current_layout.each{ |row| puts row.join('') }
  # print "\n----------------------------------------------------------------------------------------------------\n\n"

end  # do-loop

puts count_occupied_seats(current_layout)
