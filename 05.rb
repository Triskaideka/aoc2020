=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 5
https://adventofcode.com/2020/day/5
=end

passes = IO.readlines("05_input.txt", chomp: true)
seats = []

# Part 1
# "What is the highest seat ID on a boarding pass?"

highest_id = 0

passes.each do |pass|

  # error checking
  if ! pass.match?(/^[FB]{7}[LR]{3}$/) then
    puts "This pass (#{pass}) doesn't look valid."
  end

  low_seat = 0
  high_seat = 127

  left_seat = 0
  right_seat = 7

  # The problem guarantees us that we'll get seven row instructions followed by three
  # column instructions, and that's fine, but it doesn't actually matter, as long as
  # we can count on row instructions being in order with relation to one another, and
  # the same for column instructions.  It's safe to have one .each that evaluates both
  # types.
  pass.chars.each do |instr|

    # If you divide two integers, Ruby will give you an integer as the result.
    # It seems to truncate rather than round.
    # By adding .to_f to at least one side of the division, you can be sure to get a float.
    # Then we use .floor or .ceil to turn that back into the nearest integer.

    case instr
    when 'F' then
      high_seat = ((low_seat+high_seat)/2.to_f).floor
    when 'B' then
      low_seat = ((low_seat+high_seat)/2.to_f).ceil
    when 'L' then
      right_seat = ((left_seat+right_seat)/2.to_f).floor
    when 'R' then
      left_seat = ((left_seat+right_seat)/2.to_f).ceil
    else
      puts "Don't know what the instruction '#{instr}' means."
      exit
    end

  end  # each character in the boarding pass

  # assert: low_seat == high_seat and left_seat == right_seat
  if low_seat != high_seat || left_seat != right_seat then
    puts "Something went wrong with the math.  " +
         "low_seat = #{low_seat}, high_seat = #{high_seat}, left_seat = #{left_seat}, right_seat = #{right_seat}"
  end

  # Calculate the seat ID according to the formula given in the problem statement.
  seat_id = low_seat * 8 + left_seat

  seats.push(seat_id)  # this is preparation for part 2

  if seat_id > highest_id then
    highest_id = seat_id
  end

end  # each boarding pass

puts highest_id


# Part 2
# "What is the ID of your seat?"
# "Some of the seats at the very front and back of the plane don't exist on this aircraft,
# so they'll be missing from your list as well."
# "Your seat wasn't at the very front or back, though;
# the seats with IDs +1 and -1 from yours will be in your list."

# By starting with last_seat at 0 and then searching from the *top* of the list downwards,
# we make the search a little more efficient by not having to check whether we're on the
# first iteration every time through the .each loop.
# I guess we could achieve the same thing by starting with last_seat at infinity and
# searching upwards.
last_seat = 0

# Helps do a visual inspection of the answer.
#puts seats.sort.join(',')
#exit

seats.sort.reverse.each do |this_seat|

  if last_seat - 1 > this_seat then
    puts last_seat - 1
    exit
  end

  last_seat = this_seat

end
