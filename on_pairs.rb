=begin
on_pairs.rb

Adds a method called "on_pairs" to the Array class.  The method runs the given
block on each combination of two (different) elements of the array.

To clarify, the paired elements may have the same value, but not the same index.
Also, the same pair of elements will never be considered twice (e.g. in the
opposite order).  If order of elements *in the pair* matters, rewrite this
function or write a new one.  The elements *are* considered in the same order
in which they appear in the array, however.
=end
class Array
  def on_pairs

    self.each_with_index do |a, i|
      self[(i+1)..].each do |b|
        yield(a, b)
      end
    end

  end
end
