=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 1
https://adventofcode.com/2020/day/1
=end

require "./on_pairs.rb"

expense_report = IO.readlines("01_input.txt", chomp: true)

# Part 1
# "Find the two entries that sum to 2020; what do you get if you multiply them together?"
expense_report.on_pairs do |a, b|
  if (a.to_i + b.to_i == 2020) then
    puts (a.to_i * b.to_i)
    break
  end
end

# Part 2
# "What is the product of the three entries that sum to 2020?"
expense_report.each_with_index do |a, i|
  expense_report[i...].each_with_index do |b, j|
    expense_report[j...].each do |c|
       if (a.to_i + b.to_i + c.to_i == 2020) then
         puts (a.to_i * b.to_i * c.to_i)
         exit
       end
     end
  end
end
