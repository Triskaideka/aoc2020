=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 2
https://adventofcode.com/2020/day/2
=end

passwords = IO.readlines("02_input.txt", chomp: true)
record_re = /^([0-9]+)-([0-9]+)\s+([A-Za-z]):\s+(\w+)$/

# Part 1
# "How many passwords are valid according to their policies?"
#
# I put a whooooole lot of extra error-checking code in here trying to figure out why I was getting the wrong answer.
# Turns out it was something stupid, of course:  I had misread the question, and thought I was supposed to enter the
# number of *corrupted* passwords, but I'm actually supposed to enter the number of *valid* passwords.  I'm leaving
# the extra code in place because why not?  Maybe it'll help to refer back to it in the future.
num_corrupted = 0
num_valid = 0

passwords.each do |record|

  at_least = 0
  at_most = 0
  char = ""
  pw = ""
  corrupted = nil

  if ! record_re.match?(record) then
    puts "Can't parse this line:  #{record}"
    exit
  end

  record_re.match(record) do |m|

    at_least = m[1].to_i
    at_most = m[2].to_i
    char = m[3]
    pw = m[4]

    if char.length > 1 then
      puts "Unexpected character requirement (#{char})."
      exit
    end

    if ((pw.count char) < at_least) || ((pw.count char) > at_most) then
      corrupted = true
      num_corrupted += 1
    else
      corrupted = false
      num_valid += 1
    end

    # puts "#{pw} must contain from #{at_least} to #{at_most} instances of #{char}.  " +
    #      "It contains #{pw.count char}, which is #{corrupted == true ? "wrong" : "okay"}.  " +
    #      "Valid so far: #{num_valid}/#{num_valid + num_corrupted}"

    if corrupted == nil then
      puts "There's a problem here."
      exit
    end

  end  # match block

end

puts num_valid



# Part 2
# "How many passwords are valid according to the new interpretation of the policies?"
# The new interpretation is that the numbers represent indexes, not a range.
# One index or the other, but not both, must contain the indicated character.
num_corrupted = 0
num_valid = 0

passwords.each do |record|

  at_least = 0
  at_most = 0
  char = ""
  pw = ""

  record_re.match(record) do |m|

    i1 = m[1].to_i - 1  # 'Be careful; Toboggan Corporate Policies have no concept of "index zero"!'
    i2 = m[2].to_i - 1
    char = m[3]
    pw = m[4]

    if ( ( (pw.chars[i1] == char) && (pw.chars[i2] != char) ) ||
         ( (pw.chars[i1] != char) && (pw.chars[i2] == char) )
    ) then
      num_valid += 1
    else
      num_corrupted += 1
    end

  end  # match block

end

puts num_valid

