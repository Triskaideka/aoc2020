=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 23
https://adventofcode.com/2020/day/23
=end

#input = 389125467  # example input
input = 469217538  # real input

# Part 1
# "Using your labeling, simulate 100 moves. What are the labels on the cups
# after cup 1?"
#
# Part 2
# "Your labeling is still correct for the first few cups; after that, the
# remaining cups are just numbered in an increasing fashion starting from the
# number after the highest number in your list and proceeding one by one until
# one million is reached."
#
# "... the small crab isn't going to do merely 100 moves; the crab is going to
# do ten million (10000000) moves!
#
# "The crab is going to hide your stars - one each - under the two cups that
# will end up immediately clockwise of cup 1. You can have them if you predict
# what the labels on those cups will be when the crab is finished.
#
# "Determine which two cups will end up immediately clockwise of cup 1. What do
# you get if you multiply their labels together?"
#
#
#
# The key insight here was to use *both* a linked list *and* an ordered array to
# store the cups.  Using only one or the other method led to unacceptably long
# running times.  This program runs in a little over 10 seconds on my machine.
#
# This might be the first time I've been glad that Ruby assignment is by
# reference; this probably would have been a lot harder to think about if I'd
# had to be explicit about when I wanted things to be references.

# A class to represent a single cup.
# Features the cup's label, and a link to the next cup in the circle (the Circle
# class will set that link).
class Cup

  attr_accessor :label, :next

  def initialize (label)

    @label = label

    @next = nil

  end

end  # class Cup




class Circle

  # Only :current_cup is really necessary, so you can read its label from
  # outside the class, but the others might be helpful for debugging, and anyway
  # they're not harmful.
  attr_reader :current_cup, :max_label, :min_label



  def initialize (starting_list, max_label)

    first_cup = Cup.new(starting_list[0])

    # "Before the crab starts, it will designate the first cup in your list as
    # the current cup."
    @current_cup = first_cup

    # An array to easily access cups by label, regardless of where they are in
    # the circle.  Element 0 must be nil because there's no cup labelled 0.
    @ordered_cup_list = [nil]
    @ordered_cup_list[ starting_list[0] ] = first_cup

    @min_label = starting_list.min
    @max_label = max_label



    # Construct the initial circle.
    # The way that I'm constructing an array for us to iterate through here may
    # well be confusing; consult the problem statement to understand what's
    # going on.
    ( starting_list[1..] + ( (starting_list.max + 1)..max_label ).to_a ).each do |label|

      # Create a brand-new cup.
      next_cup = Cup.new(label)

      # Store the newly created cup in its appropriate slot in the
      # ordered cup list.
      @ordered_cup_list[label] = next_cup

      # Link the current cup's "next" property to the newly created cup.
      @current_cup.next = next_cup

      # Step up the current cup so we can do this again.
      @current_cup = next_cup

    end



    # Make this circle circular.
    @current_cup.next = first_cup

    @current_cup = first_cup

  end   # method initialize



  # Convenience method for moving the current cup pointer to the next cup
  # in clockwise order.
  def advance_current_cup
    @current_cup = @current_cup.next
  end



  # Make one move in the game.
  def move

    # Step 1:  "The crab picks up the three cups that are immediately clockwise
    # of the current cup. They are removed from the circle; cup spacing is
    # adjusted as necessary to maintain the circle."
    first_picked_up_cup = @current_cup.next
    @current_cup.next = first_picked_up_cup.next.next.next

    # This will (probably) change, but we want to start iterating from this
    # point.
    destination_cup = first_picked_up_cup.next.next.next



    # Step 2:  "The crab selects a destination cup: the cup with a label equal
    # to the current cup's label minus one."
    destination_cup_label = @current_cup.label - 1

    # "If at any point in this process the value goes below the lowest value
    # on any cup's label, it wraps around to the highest value on any cup's
    # label instead."
    if destination_cup_label < min_label then
      destination_cup_label = max_label
    end

    # "If this would select one of the cups that was just picked up, the crab
    # will keep subtracting one until it finds a cup that wasn't just picked
    # up."
    while destination_cup_label == first_picked_up_cup.label ||
          destination_cup_label == first_picked_up_cup.next.label ||
          destination_cup_label == first_picked_up_cup.next.next.label do

      destination_cup_label -= 1

      # Again, wrap the label around to the top if necessary.
      if destination_cup_label < min_label then
        destination_cup_label = max_label
      end

    end

    # Quickly find the destination cup in the ordered cup list.
    destination_cup = @ordered_cup_list[destination_cup_label]



    # Step 3:  "The crab places the cups it just picked up so that they are
    # immediately clockwise of the destination cup. They keep the same order as
    # when they were picked up."
    first_picked_up_cup.next.next.next = destination_cup.next
    destination_cup.next = first_picked_up_cup



    # Step 4:  "The crab selects a new current cup: the cup which is immediately
    # clockwise of the current cup."
    self.advance_current_cup

  end  # method move

end  # class Circle



# Solve part 1.
moves = 100
max_label = input.digits.max

c = Circle.new(input.digits.reverse, max_label)

# Play the game through as many moves as instructed.
moves.times do
  c.move
end

# Spin the circle until the cup labelled 1 is the current cup.
until c.current_cup.label == 1 do
  c.advance_current_cup
end

# Spin again, printing each number (other than 1) in order.
loop do
  c.advance_current_cup
  break if c.current_cup.label == 1
  print c.current_cup.label
end
print "\n"



# Solve part 2.
moves     = 10000000
max_label = 1000000

# This part starts the same.
c = Circle.new(input.digits.reverse, max_label)

# Play the game through as many moves as instructed.
moves.times do
  c.move
end

# Spin the circle until the cup labelled 1 is the current cup.
until c.current_cup.label == 1
  c.advance_current_cup
end

# Multiply the two numbers that are after the 1.
puts c.current_cup.next.label * c.current_cup.next.next.label
