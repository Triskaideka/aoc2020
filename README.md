I participated in [Advent of Code](https://adventofcode.com/) in
[2020](https://adventofcode.com/2020), using [Ruby](https://ruby-doc.org/)
as my language of choice.  I solved all but one of the challenges on or before the 25th.



# How to use these scripts

Running `ruby 01.rb` will print the solutions for both parts of the day 1
problem, each on their own line.  Replace `01` with any number up to `25` to see
the solutions for that day.

Note that AoC gives each participant individualized puzzle input.  In this
directory you're seeing my input files, and thus my solutions.  If you were to
replace these input files with your own, you should get your solutions.



# Running times

Most of these programs will find their solutions in less than a second.  Some
take a few seconds.  Day 17 takes about 50 seconds, and day 22 takes a little
over two minutes, which both exceed AoC's
[threshold](https://adventofcode.com/2020/about) of 15 seconds, but are within
the limits of what I'm personally willing to tolerate.

I consider day 13, part 2 to be unsolved.  The code I have would probably
produce the correct solution after a couple of hours, but I haven't waited.
(There is, I assume, a mathematical principle that I need to learn but don't
have the vocabulary to search for.)

For demonstration purposes, I wrote [`time_each.sh`](time_each.sh), which
executes each day's script and shows you how long it took.



# Take-aways

Other than getting a little more familiar with Ruby, what did I learn from
doing this?

One lesson is that it's important to be under the *right amount* of pressure. I
wanted to submit all of my solutions by the 25th, and without a deadline like
that, I probably wouldn't have gotten as much done during the month as I did
(and may well have abandoned the project).  But I noticed that as the problems
grew more complex and the 25th drew closer, I became less likely to try to
research unfamiliar language features or problem-solving strategies.  I leaned
more heavily on what I already knew, because new ideas could have been dead-ends
that would cost time I didn't have.  In the end, I solved (most of) the
problems, but I may have missed opportunities to learn things that would help me
solve similar problems faster in the future.

In a year when a global pandemic wasn't keeping me at home, I probably would not
have had time for a project like this at all.
