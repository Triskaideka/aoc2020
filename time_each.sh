#!/bin/bash

# A simple script to run all of the Ruby scripts in this directory and show how
# long each one takes.

for rb in ??.rb
do
  echo
  echo "----[ $rb ]-----------------"
  time ruby $rb
done
