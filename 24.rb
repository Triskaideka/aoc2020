=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 24
https://adventofcode.com/2020/day/24
=end

#instructions = IO.readlines("24_input_example.txt", chomp: true)
instructions = IO.readlines("24_input.txt", chomp: true)

# Part 1
# "Go through the renovation crew's list and determine which tiles they need to
# "flip. After all of the instructions have been followed, how many tiles are
# "left with the black side up?"
#
# The problem statement says:  "The lobby is large enough to fit whatever
# pattern might need to appear there."  In other words, it's effectively
# infinite.  I don't think we're meant to build an array (or other data
# structure) representing the entire floor.  Rather, we should build a system
# for storing individual tile information, and determining when two sets of
# instructions lead to the same tile.
#
# Insight:  an instruction such as "northeast" moves our "pointer" to the east
# by half as much as the instruction "east" does.  If we treat each tile as
# having a length of 2, it should allow us to do integer arithmetic on the tile
# addresses.


# false = white; true = black
tiles = Hash.new(false)

# Process each instruction.
instructions.each do |instr|

  # We're going to modify the contents of the instruction variable in this loop,
  # but we don't want to lose it from the original set, in case we need it
  # later.
  instr = instr.clone

  # Distance north (+) or south (-) from the reference tile.
  ns_dist = 0

  # Distance east (+) or west (-) from the reference tile.
  ew_dist = 0

  until instr.empty? do

    # Even though directions could be either one or two characters long, it's
    # never ambiguous what the next direction might be.  One-character
    # directions will always start with 'e' or 'w', and two-character
    # directions will always start with 'n' or 's'.
    m = instr.match(/^(nw|ne|sw|se|w|e)/)
    direction = m[1]

    case direction

      when 'ne'
        ns_dist += 2
        ew_dist += 1

      when 'nw'
        ns_dist += 2
        ew_dist -= 1

      when 'se'
        ns_dist -= 2
        ew_dist += 1

      when 'sw'
        ns_dist -= 2
        ew_dist -= 1

      when 'e'
        ew_dist += 2

      when 'w'
        ew_dist -= 2

    end

    # Get rid of this direction now that we've followed it.
    instr.sub!(direction, '')

  end

  # Now flip the tile at the address we've arrived at.
  tiles[[ns_dist, ew_dist]] = ! tiles[[ns_dist, ew_dist]]

end  # each instruction



# Given a list of tiles, return the number that are black (true).
def count_black_tiles (arrangement)

  return arrangement
          .values
          .reduce(0){ |num_black_tiles, color|
            num_black_tiles + (color ? 1 : 0)
          }

end  # method count_black_tiles



puts count_black_tiles(tiles)



# Part 2
# "The tile floor in the lobby is meant to be a living art exhibit. Every day,
# "the tiles are all flipped according to the following rules:
#    - Any black tile with zero or more than 2 black tiles immediately adjacent
#      to it is flipped to white.
#    - Any white tile with exactly 2 black tiles immediately adjacent to it is
#      flipped to black."
#
# "How many tiles will be black after 100 days?"

num_days = 100



# Return coordinates for the six neighbors of the tile whose coordinates are
# passed in.
def neighboring_coordinates_of (coords)

  ns_dist = coords[0]
  ew_dist = coords[1]

  return [

    # ne
    [ ns_dist + 2, ew_dist + 1 ],

    # nw
    [ ns_dist + 2, ew_dist - 1 ],

    # se
    [ ns_dist - 2, ew_dist + 1 ],

    # sw
    [ ns_dist - 2, ew_dist - 1 ],

    # e
    [ ns_dist, ew_dist + 2 ],

    # w
    [ ns_dist, ew_dist - 2 ]

  ]

end  # method neighboring_coordinates_of



def perform_daily_flip (current_arrangement)

  new_arrangement = current_arrangement.clone



  # The problem here is that there are many white tiles that we haven't been
  # keeping track of in the "tiles" hash.  So now we have to identify any
  # white tile we might care about.
  white_neighbors = Hash.new(0)



  # Look through all of the tiles that we've ever flipped.
  current_arrangement.each_pair do |coordinates, color|

    # If this tile is white, skip it.
    next if ! color

    # Count the number of black tiles adjacent to this black tile.
    adjacent_black_tiles = neighboring_coordinates_of(coordinates)
                            .reduce(0){ |num_black_tiles, neighbor|
                              if current_arrangement[neighbor] then
                                num_black_tiles + 1
                              else
                                num_black_tiles
                              end
                            }

    # "Any black tile with zero or more than 2 black tiles immediately adjacent
    # to it is flipped to white."
    unless (1..2).include?(adjacent_black_tiles) then
      new_arrangement[coordinates] = false
    end

    # Count the appearance of each white tile that neighbors this black tile.
    # This feels kind of inefficient, but the program runs in about six seconds,
    # which is fine.  I still like that the technique we're using to store tile
    # states spares us from storing tiles that have never been flipped.
    neighboring_coordinates_of(coordinates).each do |coords|

      if ! current_arrangement[coords] then
        white_neighbors[coords] += 1
      end

    end

  end



  # "Any white tile with exactly 2 black tiles immediately adjacent to it is
  # flipped to black."
  #
  # Any white tile that we've seen exactly twice in neighbors lists must have
  # exactly two black neighbors.
  white_neighbors.each_pair do |coords, num_black_neighbors|

    if num_black_neighbors == 2 then
      new_arrangement[coords] = true
    end

  end



  return new_arrangement

end  # method perform_daily_flip



# Work through the number of days required by the puzzle.
(1..num_days).each do |day|

  tiles = perform_daily_flip(tiles)

  # Uncomment if you want to periodically output the count of black tiles like
  # in the problem statement.
  #if day < 10 || day % 10 == 0 then
  #  puts "Day #{day}: #{count_black_tiles(tiles)}"
  #end

end

puts count_black_tiles(tiles)
