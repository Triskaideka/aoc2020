=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 17
https://adventofcode.com/2020/day/17

The original day 17, part 1 program was written in a way that was not easily
extensible to solve part 2 of the problem.  This program is a reimplementation.

Whereas the part 1 program stored cubes in a three-dimensional hash, this
program takes the approach of storing all cubes in a one-dimensional hash,
indexed by arrays that represent a set of coordinates.  This approach allows
for "pocket dimensions" with any number of spatial dimensions (axes).  As long
as we always use the same order for the axes in these index arrays, it turns
out we don't really need to label *which* axis is x, y, etc.

This program takes about 50 seconds to solve both parts of the puzzle.  It may
be that there is an even faster solution, but 50 seconds is good enough for me.
=end

#initial_state = IO.readlines("17_input_example.txt", chomp: true)
initial_state = IO.readlines("17_input.txt", chomp: true)

initial_state.map!{ |vector| vector.chars }



# Part 1
# "Starting with your given initial configuration, simulate six cycles.
# How many cubes are left in the active state after the sixth cycle?"
#
# Part 2
# "Starting with your given initial configuration, simulate six cycles in a
# 4-dimensional space. How many cubes are left in the active state after the
# sixth cycle?"

num_cycles = 6



# Class to represent a single cube ("hypercube", whatever) that sits at a
# certain point in the pocket dimension.
class Cube

  # Let outsiders get and set the cube's properties.
  #
  # There should be no need to change the coordinates of a cube once it's
  # created.  Cubes don't move.  They stay in the same place, and only their
  # state changes.
  attr_accessor :state
  attr_reader :coordinates, :neighbors

  def initialize (coords = [0], active = false)

    # We rely on the PocketDimension class to send the right number of
    # coordinates.  The default value for any coordinate not sent will be 0.
    @coordinates = Array.new(0, 0)
    coords.each do |c|
      @coordinates.push(c)
    end

    @neighbors = self.list_neighbors  # list_neighbors is expensive; let's try to only call it once
    @state = active

  end



  # Get a list of the addresses of the given cube's neighbors.
  #
  # Returns an array which contains sub-arrays which contain coordinates.
  # The sub-arrays can be passed as arguments to something like .cube() in the
  # PocketDimension class.
  def list_neighbors

    # An array of offsets for each coordinate.
    modification = Array.new(@coordinates.length, -1)

    neighbors = []

    # The value of "modification" will become false once it's run through all
    # the possibilities.
    while modification != false

      new_neighbor = []

      # Produce a new set of coordinates that is the result of modifying what's
      # in "@coordinates" by what's in "modification".
      (0...@coordinates.length).each do |i|
        new_neighbor[i] = @coordinates[i] + modification[i]
      end

      # Add the new set of coordinates to our list of neighbors.
      neighbors.push(new_neighbor)

      # Move on the the next offsets.
      modification = tick(modification)

      # If "modification" is all zeroes, these aren't the coordinates of a
      # neighbor, they're the coordinates of the cube itself.  So move on to
      # the next tick.
      if modification && modification.index{ |c| c != 0 }.nil? then
        modification = tick(modification)
      end

    end

    return neighbors

  end  # method list_neighbors



  # Increase the numbers in the given array, odometer-style, between -1 and 1.
  #
  # Helper method for .list_neighbors.
  #
  # This is all very complicated; it feels like there should be a simpler
  # solution.  But it does work.
  def tick (coords)

    # Check for errors.
    if coords === false then
      raise "Stop trying to tick up an odometer that's already hit its maximum."
    end

    i = coords.length - 1

    while i >= 0

      if coords[i] >= 1 then

        coords[i] = -1
        i -= 1

      else

        coords[i] += 1
        break

      end

    end

    if i < 0 then
      coords = false
    end

    return coords

  end  # method tick



  # Display as a string.
  def to_s

    if @state then
      return '#'
    else
      return '.'
    end

  end

end



# Class that represents the whole pocket dimension.
class PocketDimension

  # This is necessary for the .cycle method, in which we assign the @space
  # from a temporary PocketDimension back to self's @space.
  attr_reader :space



  def initialize (num_spatial_dimensions = 3, slice = [[]])

    # Error checking
    if num_spatial_dimensions.class != Integer then
      raise "First argument for a new PocketDimension should be an integer."
    end

    if  ! slice.class === Array ||
        ! slice[0].class === Array
    then
      raise "Second argument for a new PocketDimension should be a two-dimensional array."
    end



    @axes = num_spatial_dimensions

    # The "space" is a hash of cubes, which have a number of
    # coordinates equal to the number of spatial dimensions, all set to 0, and
    # an initial state of false.
    #
    # The cubes hold their own coordinates, but @space is also indexed by that
    # same set of coordinates.
    @space = Hash.new( Cube.new( Array.new(@axes, 0) ) )

    # Create cubes as instructed by the initial slice.
    slice.each_with_index do |row, y|
      row.each_with_index do |c, z|

        self.cube(
          Array.new(@axes - 2, 0).push(y).push(z),
          ( (c === '#') ? true : false)
        )

      end
    end

  end  # method initialize



  # Report the total number of active cubes in the dimension.
  def count_active_cubes

    @space.values.reduce(0){ |memo, cube|
      if cube.state then
        memo + 1
      else
        memo
      end
    }

  end  # method count_active_cubes



  # Return the number of active cubes that neighbor the specified cube.
  def count_active_neighbors (coords)

    return self.cube(coords).neighbors.reduce(0){ |active_neighbors, neighbor|

      if self.cube(neighbor).state == true then
        active_neighbors + 1
      else
        active_neighbors
      end

    }

  end



  # Getter and setter for a specific cube.
  # Use this method in preference to traversing @space, if (a) you know the
  # cube's coordinates, and/or (b) you don't know whether the cube already
  # exists in @space.
  def cube (coords, active = nil)

    # If the cube at these coordinates doesn't already exist, vivify it.
    if ! @space.has_key?(coords) then
      @space[coords] = Cube.new(coords)
    end

    selected_cube = @space[coords]

    # If the "active" argument was sent, set the cube's state.
    # If it wasn't sent -- and thus has its default value of "nil" -- then don't
    # change the cube's state.
    if active === true || active === '#' then
      selected_cube.state = true
    elsif active === false || active === '.' then
      selected_cube.state = false
    end

    # Return the cube.
    return selected_cube

  end  # method cube()



  # "During a cycle, all cubes simultaneously change their state according to
  # the following rules:
  #   - If a cube is active and exactly 2 or 3 of its neighbors are also active,
  #     the cube remains active. Otherwise, the cube becomes inactive.
  #   - If a cube is inactive but exactly 3 of its neighbors are active, the
  #     cube becomes active. Otherwise, the cube remains inactive."
  def cycle

    # Create a temporary PocketDimension to store the new state
    newdim = PocketDimension.new()

    self.list_all_cube_coords.each do |coords|

      # If the cube is active
      if self.cube(coords).state then

        # unless the cube has either 2 or 3 active neighbors
        unless (2..3).include?( self.count_active_neighbors(coords) ) then
          newdim.cube(coords, false)
        else
          newdim.cube(coords, true)
        end

      # If the cube is inactive
      else

        if self.count_active_neighbors(coords) === 3 then
          newdim.cube(coords, true)
        end

      end

    end  # each set of coordinates

    # Assign the space from the temporary PocketDimension back to this one
    # (after which we can safely forget the temporary one).
    @space = newdim.space

  end  # method cycle



  # Returns an array containing (as sub-arrays) the coordinates of all cubes
  # that have ever been assigned, as well as all cubes neighboring one that's
  # been assigned.  This method may be computationally expensive.
  def list_all_cube_coords

    cube_list = []

    @space.values.each do |cube|
      # Concatenate the arrays.  There will be a lot of duplicates, but we'll
      # eliminate them before returning.
      cube_list += cube.neighbors
    end

    # I was originally using |= (union assignment) in the loop above, but
    # building one big array and then calling .uniq on it is faster  -- some
    # low-effort benchmarking reveals that the difference is noticeable.
    return cube_list.uniq

  end

end  # class PocketDimension



# Solve part 1.
pd1 = PocketDimension.new(3, initial_state)

num_cycles.times do
  pd1.cycle
end

puts pd1.count_active_cubes



# Solve part 2.
pd2 = PocketDimension.new(4, initial_state)

num_cycles.times do
  pd2.cycle
end

puts pd2.count_active_cubes
