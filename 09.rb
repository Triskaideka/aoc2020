=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 9
https://adventofcode.com/2020/day/9
=end

require "./on_pairs.rb"

input_stream = IO.readlines("09_input.txt", chomp: true)
input_stream.map!(&:to_i)  # they're all numbers, so treat them that way
input_stream_2 = Marshal.load(Marshal.dump(input_stream))  # make an independent copy; we're going to need it for part 2

# Part 1
# "The first step of attacking the weakness in the XMAS data is to find the
# first number in the list (after the preamble) which is not the sum of two of
# the 25 numbers before it. What is the first number that does not have this
# property?"

buffer = []
preamble_len = 25

# Initialize the buffer.
preamble_len.times do
  buffer << input_stream.shift
end

while ! input_stream.empty? do

  components_exist = false  # flag
  next_item = input_stream.shift

  # Loop through each set of two elements in the buffer.
  buffer.on_pairs do |a, b|

    if a + b == next_item then
      components_exist = true
      break
    end

  end

  # If we considered the entire buffer and didn't find any two
  # items that can sum to next_item, then it's our winner.
  if ! components_exist then
    not_a_sum = next_item
    break
  end

  # Move on to the next item in the stream,
  # discarding the earliest item in the buffer.
  buffer.shift
  buffer << next_item

end

puts not_a_sum



# Part 2
# "What is the encryption weakness in your XMAS-encrypted list of numbers?"

# Initialize this so it'll be available for assignment inside the block and
# reading when we're outside it again.
weakness = nil

=begin
The sets of numbers could be any size, and .on_pairs only returns sets of size
2.  However, the sets are contiguous, which means they will consist of all
elements from a certain starting index to a certain ending index.  That *is*
a set of size 2.  So we can use .on_pairs to get all the starting and ending
indexes, then use that pair of numbers to access the elements of the input
stream array.

The ... operator produces a Range, but I only added .on_pairs to the Array
class.  But it's easy enough to convert a Range to an Array with the .to_a
method.
=end
(0...input_stream_2.length).to_a.on_pairs do |start_idx, end_idx|

  contiguous_set = input_stream_2[start_idx..end_idx]

  # If the sum of this set equals the number we found in part 1, it's our target.
  if contiguous_set.sum == not_a_sum then
    weakness = contiguous_set.min + contiguous_set.max
    break
  end

end

puts weakness
