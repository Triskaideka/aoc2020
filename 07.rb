=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 7
https://adventofcode.com/2020/day/7
=end

#bag_rules = IO.readlines("07_input_example.txt", chomp: true)
#bag_rules = IO.readlines("07_input_example_2.txt", chomp: true)
bag_rules = IO.readlines("07_input.txt", chomp: true)



# Part 1
# "You have a shiny gold bag. If you wanted to carry it in at least one other bag,
# how many different bag colors would be valid for the outermost bag? (In other
# words: how many colors can, eventually, contain at least one shiny gold bag?)"

bag_colors = {}
target_color = "shiny gold"

# Make a first pass to clean up text that's extraneous to understanding the rules.
bag_rules.map!{ |rule|
  rule.gsub(/\s+bags?\.?/, '')
}
#puts bag_rules.join("\n")



# Parse the cleaned-up rules.
bag_rules.each do |rule|

  bag_color, potential_contents = rule.split(/\s+contain\s+/, 2)

  # Assume that this color can't contain the target, by default.
  bag_colors[bag_color] = {:can_contain_target => false}

  potential_contents.split(/,\s+/).each do |description|

    next if description == "no other"

    m = description.match(/^([0-9]+)\s+(.+)$/)

    num = m[1].to_i
    clr = m[2]

    bag_colors[bag_color][clr] = num

    # Prime the list of colors that can contain the target color.
    if clr == target_color then
      bag_colors[bag_color][:can_contain_target] = true
    end

  end

end
#puts bag_colors.inspect ; exit



# Now we want to loop through the list of colors continuously, updating our information about whether
# each color can contain the target color.  If one of the colors that color X can contain can, in turn,
# contain the target color, then color X itself can (eventually) contain the target color.  That's new
# information, so we have to re-evaluate the list with what we now know.  We stop when we've gone
# through the list one time without having learned anything new.

changed_during_this_iteration = true  # not really, but this gets us into the while-loop the first time

while changed_during_this_iteration do

  # assume this will be false, then change it back if it becomes true
  changed_during_this_iteration = false

  bag_colors.each do |containing_color, contents|

    #puts contents.inspect ; exit

    next if contents[:can_contain_target]  # we already know

    contents.reject{ |k,v| k == :can_contain_target }.each do |clr, num|

      #puts "considering #{clr} : #{bag_colors[clr][:can_contain_target]}"

      if bag_colors[clr][:can_contain_target] then

        contents[:can_contain_target] = true
        changed_during_this_iteration = true

      end

    end

  end

end
#puts bag_colors.inspect ; exit



puts bag_colors.reject{ |n,c| ! c[:can_contain_target] }.length



# Part 2
# "How many individual bags are required inside your single shiny gold bag?"

# We don't care about this info anymore, so let's get it out of our way.
bag_colors.each do |containing_color, contents|
  contents.delete(:can_contain_target)
end
#puts bag_colors.inspect ; exit



# define a function that we can use to investigate the bag requirements recursively
def get_num_bags_required_in (bag_colors, color)

  required = 1  # this bag itself

  # For each color that this color can contain (if any), recurse into this function again.
  #
  # I wrote this code with .each_pair after looking it up on ruby-doc.org.  Then when I
  # looked it over again, I wondered, "what's the difference between .each and
  # .each_pair?"  According to https://stackoverflow.com/a/46435776 , there is no
  # difference: "each_pair may be a clearer name", but "they share the same source code."
  bag_colors[color].each_pair do |c,n|

    #puts "#{c} * #{n} = #{get_num_bags_required_in(bag_colors, c) * n}"
    required += get_num_bags_required_in(bag_colors, c) * n

  end

  return required

end



# Subtract 1 because this counts the outmost bag, but the question
# asks how many bags are required *inside* the outmost bag.
puts get_num_bags_required_in(bag_colors, target_color) - 1
