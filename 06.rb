=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 6
https://adventofcode.com/2020/day/6
=end

answers = IO.readlines("06_input.txt", chomp: true)

# Part 1
# "For each group, count the number of questions to which ANYONE answered 'yes'.
# What is the sum of those counts?"
#
# Part 2
# "For each group, count the number of questions to which EVERYONE answered 'yes'.
# What is the sum of those counts?"
#
# Not doing separate sections for the two parts of today's puzzle, because they
# both have to loop through the input, so it's more efficient to calculate both
# parts in one loop.

# Create a Hash with a key for each letter of the alphabet, with all values set to true.
# I don't fully understand the three different creation syntaxes that I had to use when
# putting this line together, but here are some references for where I learned about them:
#   https://ruby-doc.org/core-2.4.0/Array.html#method-i-zip
#   https://stackoverflow.com/questions/5174913/combine-two-arrays-into-hash
#   https://stackoverflow.com/questions/191329/correct-way-to-populate-an-array-with-a-range-in-ruby
default_true = Hash[Array('a'..'z').zip(Array.new(26, true))]
#puts default_true.inspect ; exit

groups_any = [{}]  # start with an empty hash
groups_all = [default_true.clone]  # start with a clone of our hash that's all true values
group = 0

answers.each do |answer|

  if answer.empty? then
    group += 1
    groups_any[group] = {}
    groups_all[group] = default_true.clone
    next
  end

  # Check whether anyone answered yes to this question, for part 1.
  answer.chars.each do |q|
    groups_any[group][q] = true
  end

  # Check whether anyone DIDN'T answer yes to this question, for part 2.
  default_true.keys.each do |q|
    if ! answer.chars.include?(q) then
      groups_all[group][q] = false
    end
  end

  #puts groups_all.inspect ; exit

end  # each answer

# The problem statement doesn't specifically say that this is safe.
# Could there be any groups that don't answer "yes" to any questions?
# Fortunately, it wouldn't change our solutions if there were --
# so these lines are unnecessary, but also harmless.
groups_any.reject!(&:empty?)
groups_all.reject!(&:empty?)

# If these aren't the same, something weird happened.
#puts groups_any.length
#puts groups_all.length

# Since we didn't add a key for any question to which nobody in the group answered yes,
# we know that the length of the answer hash is the number of yeses.
puts groups_any.reduce(0) { |memo, yeses| memo + yeses.length }

# In this case, every answer hash has all 26 keys, so we have to filter out the ones
# with false values before we count them.
puts groups_all.reduce(0) { |memo, yeses| memo + yeses.reject{ |q,a| !a }.length }
