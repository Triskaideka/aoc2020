=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 18
https://adventofcode.com/2020/day/18
=end

#homework = IO.readlines("18_input_example.txt", chomp: true)
homework = IO.readlines("18_input.txt", chomp: true)

require './numeric.rb'  # for String#numeric?



# Part 1
# "Evaluate the expression on each line of the homework;
# what is the sum of the resulting values?"
#
# I have a feeling that the optimal solution for this problem involves
# employing a lexer, but it was easy enough to solve without one.

# Calculate a string, which is assumed to contain a mathematical equation
# with no parentheses, in order from left to right.
def calculate (equation)

  terms = equation.split(/\s+/)

  # We're calling methods that we got from user input here, so let's be a little
  # careful about it.  These are the operators that the puzzle says to expect.
  safe_operators = [:+, :*]

  operator = :+
  memo = 0

  while term = terms.shift do

    if term.numeric? then

      # .public_send lets you apply to the object a method name that you have
      # stored in a symbol
      memo = memo.public_send(operator, term.to_i)

    elsif safe_operators.include?(operator) then

      operator = term.to_sym

    end

  end

  return memo

end  # method calculate



# Calculate a string which may contain some parenthetical subsets.
# The second argument, with_precedence, lets us call a different calculation
# method for parts 1 and 2 of the problem.
def ordered_calculate (eq, with_precedence = false)

  # This is so stupid; why aren't method arguments always passed by value??
  equation = eq.clone

  # Use a different calculate method depending on which part of this program
  # called this method.
  if with_precedence then
    alias calc calculate_with_precedence
  else
    alias calc calculate
  end

  # Repeatedly replace parenthesized sub-expressions with the value that they
  # evaluate to, until there are no such sub-expressions left in the equation.
  while equation.index(')') do

    # This regex is pretty hard to read.  It finds parenthesized sub-expressions
    # that don't, themselves, contain any parentheses, and captures the part
    # inside the parentheses as $1.
    equation.gsub!(/\(([^()]+)\)/) {
      calc($1).to_s
    }

  end

  return calc(equation)

end

puts homework.reduce(0){ |memo, eq| memo + ordered_calculate(eq) }



# Part 2
# "What do you get if you add up the results of evaluating the homework problems
# using these new rules (under which addition is evaluated before
# multiplication)?"

# Calculate a string, which is assumed to contain a mathematical equation
# with no parentheses, using an order of operations where addition comes before
# multiplication.
def calculate_with_precedence (equation)

  terms = equation.split(/\s+/)
  terms.map!{ |t| if t.numeric? then t.to_i else t.to_sym end }

  # Do this procedure for each of the acceptable operators in order.
  [:+, :*].each do |op|

    # While this operator still exists somewhere in the terms array,
    # replace it and the terms on either side of it with the result of that
    # operation.
    while terms.index(op) do

      i = terms.index(op)
      terms[ (i-1)..(i+1) ] = terms[i-1].public_send(terms[i], terms[i+1])

    end

  end

  # There should be only one term left, so just return it.
  return terms[0]

end  # method calculate

puts homework.reduce(0){ |memo, eq| memo + ordered_calculate(eq, true) }
