=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 22
https://adventofcode.com/2020/day/22

This code solves part 1 and the example inputs quickly, but takes over 2 minutes
to solve part 2 of the real problem.  Am I missing a way to shortcut it?
=end

#decks = IO.readlines("22_input_example.txt", chomp: true)

# This input intentionally causes an infinite loop if the part 2 rules for
# preventing such loops are not followed.  Comment out the part 1 code if you
# want to test this.
#decks = IO.readlines("22_input_example_2.txt", chomp: true)

decks = IO.readlines("22_input.txt", chomp: true)



# Part 1
# "Play the small crab in a game of Combat using the two decks you just dealt.
# What is the winning player's score?"

# Parse the input
player = 0
players = Array.new(0){Array.new(0)}

decks.each do |card|

  if card.match?(/^Player ([0-9]+):\s*$/) then
    card.match(/^Player ([0-9]+):\s*$/)
    player = $1.to_i - 1
    players[player] = []
    next
  end

  next if card.empty?

  players[player] << card.to_i

end

# Save these for part 2.
starting_deck_1 = players[0].clone
starting_deck_2 = players[1].clone



# Play the game until all but one player have been eliminated.
# Having been burned by previous puzzles, I chose to write this code in a way
# that allowed for the game to have any number of players.
while players.length > 1 do

  trick = []

  # Play the top card of each player's deck.
  players.each do |deck|
    trick << deck.shift
  end

  # Which card is the highest one in the trick?
  high_card = trick.max

  # This will give us the player number of the player who won this trick.
  winner = trick.find_index(high_card)

  # Add all of the cards in the trick to the bottom of the winner's deck, in
  # descending order.
  players[winner] += trick.sort.reverse

  # If any players have run out of cards, remove them from the game.
  players.reject!{ |deck| deck.length < 1 }

end

# Add up the winning (i.e. only remaining) player's score according to the rules
# of the game.
i = 0
puts players[0].reverse.reduce(0){ |memo, card|
  i += 1
  memo + (i * card)
}



# Part 2
# "Defend your honor as Raft Captain by playing the small crab in a game of
# Recursive Combat using the same two decks as before. What is the winning
# player's score?"

# Play one game, or sub-game, of "Recursive Combat".
# Return the state of the two decks after the game ends.  You can tell which
# player won because their deck will be the one that isn't empty.
#
# Now that I know part 2 of this puzzle doesn't involve more than two players,
# I'm hard-coding the assumption that the game has two players, because it's
# easier for me to think about that way.
def play_combat (deck1, deck2)

  previously_seen_decklists = []

  until deck1.empty? || deck2.empty? do

    # "Before either player deals a card, if there was a previous round in this
    # game that had exactly the same cards in the same order in the same
    # players' decks, the game instantly ends in a win for player 1. Previous
    # rounds from other games are not considered. (This prevents infinite games
    # of Recursive Combat, which everyone agrees is a bad idea.)
    if previously_seen_decklists.include?([deck1, deck2]) then
      # The actual contents of the decks we return in this case are not
      # important; we just need to know who the winner is.  The contents of the
      # decks returned are only important on the outermost method call.
      return [1], []
    end

    previously_seen_decklists.push([deck1.clone, deck2.clone])



    # Draw the top card of each deck.
    drawn1 = deck1.shift
    drawn2 = deck2.shift

    # "If both players have at least as many cards remaining in their deck as
    # the value of the card they just drew, the winner of the round is
    # determined by playing a new game of Recursive Combat."
    if deck1.length >= drawn1 && deck2.length >= drawn2 then

      subdeck1, subdeck2 = play_combat(deck1[0...drawn1], deck2[0...drawn2])

      if subdeck1.empty? then
        winner = 2
      else
        winner = 1
      end

    # "Otherwise, at least one player must not have enough cards left in their
    # deck to recurse; the winner of the round is the player with the
    # higher-value card."
    else

      if drawn1 > drawn2 then
        winner = 1
      else  # drawn2 > drawn1
        winner = 2
      end

    end



    # Put both drawn cards on the bottom of the winner's deck, winner's card
    # first (regardless of whether it's higher or not).
    if winner == 1 then
      deck1 << drawn1 << drawn2
    else  # winner == 2
      deck2 << drawn2 << drawn1
    end

  end  # until one deck is empty



  return deck1, deck2

end  # play_combat



# Add up the winning (i.e. only remaining) player's score according to the rules
# of the game.  (This is much the same code as in part 1; just formatted
# differently.)
i = 0
puts play_combat(starting_deck_1, starting_deck_2)
      .select{ |deck| ! deck.empty? }[0]
      .reverse
      .reduce(0){ |memo, card|
        i += 1
        memo + (i * card)
      }
