=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 25
https://adventofcode.com/2020/day/25
=end

#public_keys = IO.readlines("25_input_example.txt", chomp: true)
public_keys = IO.readlines("25_input.txt", chomp: true)

# Part 1
# "What encryption key is the handshake trying to establish?"

card_pub_key = public_keys[0].to_i
door_pub_key = public_keys[1].to_i

card_loop_size = 1
door_loop_size = 1



# Given a subject number and a public key, work out how many loops are required
# to transform the starting value 1 into that public key.
def divine_loop_size (subject_number, public_key)

  value = 1
  loop_size = 0

  while value != public_key do

    loop_size += 1

    value *= subject_number
    value %= 20201227

  end

  return loop_size

end  # method divine_loop_size



# "To transform a subject number, start with the value 1. Then, a number of
# times called the loop size, perform the following steps:
#    - Set the value to itself multiplied by the subject number.
#    - Set the value to the remainder after dividing the value by 20201227."
def transform (subject_number, loop_size)

  value = 1

  loop_size.times do

    value *= subject_number
    value %= 20201227

  end

  return value

end  # method transform



# Find the two loop sizes.
card_loop_size = divine_loop_size(7, card_pub_key)
door_loop_size = divine_loop_size(7, door_pub_key)

# Work out the encryption key (either of these should produce the same result).
puts transform(door_pub_key, card_loop_size)
#puts transform(card_pub_key, door_loop_size)
