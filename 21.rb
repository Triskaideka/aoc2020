=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 21
https://adventofcode.com/2020/day/21
=end

#food_list = IO.readlines("21_input_example.txt", chomp: true)
food_list = IO.readlines("21_input.txt", chomp: true)

# Part 1
# "Determine which ingredients cannot possibly contain any of the allergens in
# your list. How many times do any of those ingredients appear?"
#
# Relevant information: "Each allergen is found in exactly one ingredient. Each
# ingredient contains zero or one allergen. Allergens aren't always marked; when
#  they're listed (as in (contains nuts, shellfish) after an ingredients list),
# the ingredient that contains each listed allergen will be somewhere in the
# corresponding ingredients list. However, even if an allergen isn't listed, the
#  ingredient that contains that allergen could still be present: maybe they
# forgot to label it, or maybe it was labeled in a language you don't know."

allergens = []
foods = []
ingredients = []

# Go through the food list once to identify the complete lists of ingredients
# and allergens.
food_list.each do |food|

  m = food.match(/^(.+)\s+\(contains\s+(.+)\)/)

  these_ingredients = m[1].split(/\s+/)
  these_allergens = m[2].split(/[, ]+/)

  foods.push({:ingredients => these_ingredients, :allergens => these_allergens})

  # "|=" is the union assignment operator.  It adds to the left array
  # anything from the right array that it doesn't already have.
  ingredients |= these_ingredients
  allergens |= these_allergens

end



# Keys are allergen names.
# Values are arrays listing the ingredients that the allergen *could* be found in.
ingredients_with = Hash.new([])

# Narrow down the list of ingredients that each allergen *might* be present in.
allergens.each do |allergen|

  ingredients_with[allergen] = ingredients.select{ |ingredient|

    # Select the list of foods that are known to include this allergen.
    foods.select{ |food|

      food[:allergens].include?(allergen)

    # Now disqualify any ingredients that *don't* appear in *every* food in that
    # list.
    }.index{ |food|

      ! food[:ingredients].include?(ingredient)

    }.nil?

  }

end



# Make a list of the ingredients that we can be certain contain no allergens.
non_allergenic_ingredients = ingredients.reject{ |ing|

  # Start by assuming that we don't want to reject this ingredient.
  flag = false

  ingredients_with.values.each do |ingredient_list|

    # *Do* reject this ingredient if it appears anywhere in "ingredients_with".
    if ingredient_list.include?(ing) then
      flag = true
      break
    end

  end

  # Return the value that the flag has been set to.
  flag

}



# Count the number of times any non-allergenic ingredient is listed in the
# original food list.
puts foods.reduce(0){ |memo, food|
  memo + food[:ingredients].select{ |ing|
    non_allergenic_ingredients.include?(ing)
  }.length
}



# Part 2
# "Arrange the ingredients alphabetically by their allergen and separate them by
# commas to produce your canonical dangerous ingredient list. (There should not
# be any spaces in your canonical dangerous ingredient list.)"
#
# "What is your canonical dangerous ingredient list?"

dangerous = {}

# This logic for reducing possibilities is similar in principle to logic used in
# the day 16 program, although the data structures are different.  We go through
# and identify allergens that only have one ingredient they can possibly be in.
# We make a note of that, remove that ingredient from all possibility lists,
# and repeat the process with the new, smaller possibility lists, until we've
# emptied all possibility lists in this manner.
while ! ingredients_with.values.index{ |ingredient_list| ingredient_list.length > 1 }.nil? do

  ingredients_with.each do |allergen, ingredient_list|

    if ingredient_list.length == 1 then

      ing = ingredient_list[0]

      # Record the knowledge of which ingredient contains this allergen.
      dangerous[allergen] = ing

      # Dis-associate this ingredient from all allergens that we now know it
      # cannot contain.
      ingredients_with.each do |allergen, possibilities|
        possibilities.reject!{ |p| p === ing }
      end

    end

  end

end

puts dangerous.keys.sort.map{ |allergen| dangerous[allergen] }.join(',')
