=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 13
https://adventofcode.com/2020/day/13
=end

#notes = IO.readlines("13_input_example.txt", chomp: true)
notes = IO.readlines("13_input.txt", chomp: true)

# Part 1
# "What is the ID of the earliest bus you can take to the airport
# multiplied by the number of minutes you'll need to wait for that bus?"

require 'bigdecimal'  # for Infinity
require './numeric.rb'  # for String#numeric?



class Integer

  # "gte" stands for "greater than or equal to"
  def lowest_multiple_gte (target)

    # error checking
    if (target - (self * 2)) >= (target - self) then
      raise "Multiples of #{self} are never going to get closer to #{target}."
    end

    multiplier = 1

    while self * multiplier < target do
      multiplier += 1
    end

    return self * multiplier

  end  # method lowest_multiple_greater_than()
end  # class Integer

chosen_bus = nil
wait_time = BigDecimal::INFINITY

earliest_time = notes[0].to_i
bus_ids = notes[1].split(/,/).select(&:numeric?).map(&:to_i)

bus_ids.each do |id|

  if id.lowest_multiple_gte(earliest_time) - earliest_time < wait_time then
    chosen_bus = id
    wait_time = id.lowest_multiple_gte(earliest_time) - earliest_time
  end

end

puts chosen_bus * wait_time

puts "Unfinished; part 2 would take hours." ; exit




# Part 2
# "What is the earliest timestamp such that all of the listed bus IDs depart
# at offsets matching their positions in the list?"

# This approach is much more elegant than my first attempt, and it will probably
# work eventually, but it takes an unacceptably long amount of time to run.
#
# I notice that all of the bus IDs in the input file are prime numbers.
# That's probably significant, but I don't know in what way.

timetable = []

# Make a list of the buses, containing their route durations and the order in
# which they must arrive, excluding the wildcards in the arrival order.
notes[1].split(/,/).each_with_index do |bus_id, offset|

  if ( bus_id.numeric? ) then
    timetable.push( { duration: bus_id.to_i, offset: offset } )
  end

end

# Order the list in descending order of route duration.
# This lets us easily find the bus with the longest route (at the beginning)
# and lets us test the buses in order of how likely they are to fail.
timetable.sort!{ |a,b| a[:duration] <=> b[:duration] }.reverse!

offset_of_longest_route = timetable[0][:offset]

# Make every arrival time relative to that of the longest route.
timetable.each do |bus|
  bus[:offset] -= offset_of_longest_route
end



# timestamp
ts = 0

loop do

  ts += timetable[0][:duration]

  # If there's NO bus whose offset from the timestamp ISN'T a multiple of its
  # route duration, then we've found the winning timestamp.
  #
  # We can start testing the list at its second element because we already
  # know the first element will pass, since we just set the timestamp to a
  # multiple of it.
  if timetable[1..]
      .index{ |bus| (ts + bus[:offset]) % bus[:duration] != 0 }
      .nil?
  then
    break
  end

end

# Need to take the offset back out of the timestamp, because the problem asks
# for the timestamp when the first bus in the list will arrive.
puts ts - offset_of_longest_route
