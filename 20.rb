=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 20
https://adventofcode.com/2020/day/20
=end

require './on_pairs.rb'

#input = IO.readlines("20_input_example.txt", chomp: true)
input = IO.readlines("20_input.txt", chomp: true)



# Part 1
# "Assemble the tiles into an image. What do you get if you multiply together
# the IDs of the four corner tiles?"

# "Specification" is not a great name for this (or for anything, really).  It's
# a combination of a tile ID and an orientation number.
#
# Do we even need to use a Struct for this?  Previously I was using a hash with
# symbol keys. The Struct approach doesn't change the code or the running time
# very much at all.  But oh well; now I've learned something about Structs, and
# maybe it will be useful to look back at this code in the future.
Specification = Struct.new(:id, :orientation)



# A "Layout" is one particular grid of # and . characters.
class Layout

  attr_accessor :fits_above, :fits_left_of
  attr_reader :grid



  # The "lines" argument can be either an array of strings,
  # or a two-dimensional array.
  def initialize (lines = [])

    @grid = Array.new(0){Array.new}

    lines.each do |line|
      @grid.push( line.class == String ? line.chars : line )
    end

    # Initialize these as empty for now; we'll fill them in later.
    @fits_above = []
    @fits_left_of = []

  end



  # Return the bottom border of this Layout (for comparing with the top
  # border of another Layout).
  def bottom_border
    return @grid.last
  end



  # Return a new Layout that is the result of flipping this Layout on its
  # horizontal axis.
  def flip_on_h_axis
    return Layout.new(@grid.reverse)
  end



  # Return a new Layout that is the result of flipping this Layout on its
  # horizontal axis.
  #
  # I didn't end up using this method, but whatever; it's still accurate and
  # could *theoretically* be useful.  No need to delete it.
  def flip_on_v_axis

    flipped = Array.new(0)

    @grid.each do |row|
      flipped << row.reverse
    end

    return Layout.new(flipped)

  end



  # Return the left border of this Layout (for comparing with the right
  # border of another Layout).
  def left_border
    return @grid.map{ |row| row.first }
  end



  # Return the right border of this Layout (for comparing with the left
  # border of another Layout).
  def right_border
    return @grid.map{ |row| row.last }
  end



  # Return a new Layout that is the result of turning this Layout clockwise
  # once.
  def rotate_clockwise

    rotated = Array.new(0)

    (0...@grid.length).each do |x|

      # I don't understand why I have to do this here instead of setting a
      # default object for the "rotated" array, but I've tried it several
      # different ways and this is the first one that has worked.
      rotated[x] = Array.new(0)

      (@grid.length - 1).downto(0) do |y|
        rotated[x] << @grid[y][x]
      end

    end

    return Layout.new(rotated)

  end



  # Return the top border of this Layout (for comparing with the bottom
  # border of another Layout).
  def top_border
    return @grid.first
  end



  # Output this layout for visual inspection.
  def to_s
    return @grid.map{ |row| row.join('') }.join("\n")
  end



  # Return the inner part of the layout, without its borders.  For part 2 of the
  # problem.
  def without_borders
    return @grid[1..-2].map{ |row| row[1..-2] }
  end

end  # class Layout



# A "Tile" collects all of the ways that a Layout can be flipped or rotated,
# plus its ID number.
class Tile

  attr_accessor :cur_ori_idx  # the index of the orientations array, indicating which orientation is "current"
  attr_reader :id, :orientations

  def initialize (lines)

    m = lines[0].match(/Tile ([0-9]{4}):/)
    @id = m[1].to_i

    @orientations = Array.new(1, Layout.new)

    @orientations[0] = Layout.new(lines[1..])

    @orientations[1] = @orientations[0].rotate_clockwise
    @orientations[2] = @orientations[1].rotate_clockwise
    @orientations[3] = @orientations[2].rotate_clockwise

    @orientations[4] = @orientations[0].flip_on_h_axis
    @orientations[5] = @orientations[4].rotate_clockwise
    @orientations[6] = @orientations[5].rotate_clockwise
    @orientations[7] = @orientations[6].rotate_clockwise

    @cur_ori_idx = 0

  end



  # Return whichever element of @orientations is considered the "current" one.
  def current_orientation
    return @orientations[@cur_ori_idx]
  end



  # Pass through these methods to the corresponding methods for whatever
  # orientation the tile is in.
  def bottom_border
    return self.current_orientation.bottom_border
  end

  def left_border
    return self.current_orientation.left_border
  end

  def right_border
    return self.current_orientation.right_border
  end

  def top_border
    return self.current_orientation.top_border
  end



  # Outputs the tile in a way that looks like the input.
  def to_s
    return "Tile #{@id}:\n" + self.current_orientation.to_s
  end

end  # class Tile



# An unsorted collection of tiles.
tiles = []

# Parse the input.
i = 0

while i < input.length

  next_tile = []

  # Collect all of the lines until the next blank one.
  loop do
    next_tile.push(input[i])
    i += 1
    break if i >= input.length || input[i].empty?
  end

  tiles.push( Tile.new(next_tile) )

  i += 1

end



# In each layout, build a list of other layouts that it fits above or to the
# left of.  This saves a lot of time when we're trying to fit the tiles together
# -- it cut the running time from hours down to seconds.  With this list, we
# don't have to consider every tile in the next position; we can immediately cut
# the list down to only those tiles that we already know might fit.
tiles.on_pairs do |t1, t2|

  t1.orientations.each do |o1|

      t2.orientations.each_with_index do |o2, idx|

        if o1.bottom_border == o2.top_border then
          o1.fits_above << Specification.new(t2.id, idx)
        end

        if o1.right_border == o2.left_border then
          o1.fits_left_of << Specification.new(t2.id, idx)
        end

      end

  end

end



# Recursive.  Given our progress so far, try to find a tile that will fit in the
# next un-filled position in the image.
#
# While it may seem to make more sense to send the fixed_tiles argument first
# and the remaining_tiles argument second, doing this lets us set a default
# value for the fixed_tiles array, which in turn lets the original caller only
# send one argument, and be blissfully unaware of the inner workings of this
# method.
def try_image (remaining_tiles, fixed_tiles = [])

  # The success condition.
  if remaining_tiles.empty? then
    return fixed_tiles
  end



  # This should be safe because if the length of the tiles array isn't a square
  # number, we won't be able to assemble the tiles into a square grid.
  tiles_per_row = Integer.sqrt(fixed_tiles.length + remaining_tiles.length)



  # If this tile is below the top row and/or right of the leftmost column of the
  # image, then we can eliminate a lot of possibilites for the next layout based
  # on legwork we've done earlier.
  #
  # The code in this section isn't pretty; finding ways to shorten it wouldn't
  # be terrible.
  if fixed_tiles.length % tiles_per_row != 0 then

    if fixed_tiles.length >= tiles_per_row then

      layouts_that_fit =
        fixed_tiles[-tiles_per_row].current_orientation.fits_above &
        fixed_tiles.last.current_orientation.fits_left_of

    else

      layouts_that_fit = fixed_tiles.last.current_orientation.fits_left_of

    end

  elsif fixed_tiles.length >= tiles_per_row then

    layouts_that_fit = fixed_tiles[-tiles_per_row]
                        .current_orientation
                        .fits_above

  else

    # No layouts fit here.
    layouts_that_fit = []

  end



  # Turn our list of possible layouts into a list of possible tiles.
  if layouts_that_fit.empty? then
    possibilities = remaining_tiles
  else
    possibilities = remaining_tiles.select{ |t|
                                      layouts_that_fit.map{ |l|
                                        l.id
                                      }.include?(t.id)
                                    }
  end



  # Explore each of the possibilities.
  possibilities.each do |tile|

    # Shouldn't we be able to use the layouts_that_fit list to reduce the
    # number of orientations we consider here?
    (0...tile.orientations.length).each do |orientation|

      # Cycle through all orientations for this tile.
      tile.cur_ori_idx = orientation

      # Does this tile fit in this position?  Start by assuming yes.
      fits = true



      # If the tile isn't on the left edge of the image, compare it with its
      # neighbor on the left.
      unless fixed_tiles.length % tiles_per_row == 0 then

        # If the borders don't match, this orientation doesn't fit.
        if tile.left_border != fixed_tiles.last.right_border then
          fits = false
          next
        end

      end



      # If the tile isn't on the top edge of the image, compare it with its
      # neighbor above it.
      if fixed_tiles.length >= tiles_per_row then

        # If the borders don't match, this orientation doesn't fit.
        if tile.top_border != fixed_tiles[-tiles_per_row].bottom_border then
          fits = false
          next
        end

      end



      # If this tile fits in this position, move it to the fixed_tiles array
      # and try one of the other remaining tiles in the next empty position.
      if fits then

        success = try_image(
                    remaining_tiles.reject{ |t| t == tile },
                    fixed_tiles + [tile]
                  )

        # If try_image didn't return false, then we must have found an
        # arrangement that works, so just keep returning it up the chain.
        # Otherwise, we'll continue on with the next orientation for this tile.
        if success != false then
          return success
        end

      end

    end  # each orientation

    # If we made it through all the orientations for this tile and none of them
    # worked, this tile must not fit in this position, so we'll try another one
    # of the remaining tiles.

  end  # each tile in the image

  # If we made it through each tile in the image and none of them fit in this
  # position in any orientation, we've gone down a dead end.
  return false

end  # method try_image



# Figure out how to organize the image.
organized_image = try_image(tiles)

tiles_per_row = Integer.sqrt(organized_image.length)



# Uncomment this if you want to see a grid of IDs like the one in the problem
# statement.  Interestingly, mine is a diagonal mirror image of the one in the
# problem statement when run on the example input, but that's fine because we
# only need to know the four corner tiles, which will be the same.
# i = 0
# organized_image.each do |tile|
#   print "#{tile.id} "
#   i += 1
#   if i % tiles_per_row == 0 then print "\n" end
# end



# Print the desired solution.
puts organized_image.first.id *
     organized_image[tiles_per_row-1].id *
     organized_image[-tiles_per_row].id *
     organized_image.last.id



# Part 2
# "How many # are not part of a sea monster?"

# Remove the borders of each tile, and stitch together the "actual image".
actual_image = Array.new(0, Array.new(0, ''))

# Note that we're not just doing "organized_image.each" because we actually use
# the value of 't' for something else in here.
(0...organized_image.length).each do |t|

  tile = organized_image[t]

  inner_image = tile.current_orientation.without_borders

  inner_image.each_with_index do |row_of_tile, i|

    # Adjust the height as needed after the first row of tiles.
    i += inner_image.length * (t / tiles_per_row)

    # Vivify the row as needed.
    if actual_image[i].nil? then
      actual_image[i] = []
    end

    # Add these rows to the image.
    actual_image[i] += row_of_tile

  end

end



# Define what a sea monster looks like.
sea_monster = [
  [-1, 18],
  [0, 0], [0, 5], [0, 6], [0, 11], [0, 12], [0, 17], [0, 18], [0, 19],
  [1, 1], [1, 4], [1, 7], [1, 10], [1, 13], [1, 16]
]

# Uncomment to print a sea monster, to verify that it looks correct.
# (-1..1).each do |r|
#   (0..19).each do |c|
#     if sea_monster.include?([r,c]) then
#       print '#'
#     else
#       print ' '
#     end
#   end
#   print "\n"
# end



# Construct all of the possible orientations of the image that we'll need to
# look through.
actual_image_layouts = []

# Turning this array back into a Layout will let us take advantage of the
# methods in the Layout class.
actual_image_layouts[0] = Layout.new(actual_image)

actual_image_layouts[1] = actual_image_layouts[0].rotate_clockwise
actual_image_layouts[2] = actual_image_layouts[1].rotate_clockwise
actual_image_layouts[3] = actual_image_layouts[2].rotate_clockwise

actual_image_layouts[4] = actual_image_layouts[0].flip_on_h_axis
actual_image_layouts[5] = actual_image_layouts[4].rotate_clockwise
actual_image_layouts[6] = actual_image_layouts[5].rotate_clockwise
actual_image_layouts[7] = actual_image_layouts[6].rotate_clockwise



# Find sea monsters.
actual_image_layouts.each do |layout|

  num_sea_monsters = 0
  grid = layout.grid

  grid.each_with_index do |row, y|

    row.each_with_index do |cell, x|

      # If this cell has something in it (i.e. a '#' as opposed to a '.')....
      if cell == '#' then

        here_be_monsters = true  # flag



        # This could be the start of a sea monster, so let's look for the rest
        # of the sea monster.
        sea_monster.each do |coords|

          y_offset, x_offset = coords

          unless grid[y + y_offset][x + x_offset] == '#' then
            here_be_monsters = false
            break
          end

        end



        # Looks like this cell *was* the start of a sea monster.
        if here_be_monsters then

          # Reveal the sea monster in the image.
          sea_monster.each do |coords|
            y_offset, x_offset = coords
            grid[y + y_offset][x + x_offset] = 'O'
          end

          num_sea_monsters += 1

        end

      end  # if this cell has something in it

    end  # each cell in the row

  end  # each row of the grid



  # The problem statement does not directly say, but does seem to imply, that
  # only one orientation of the image will feature any sea monsters.  So, if
  # we've found any this time through, we'll assume that this orientation is
  # the only correct one.
  if num_sea_monsters > 0 then

    # Uncomment if you want to see the sea monsters.
    # puts layout
    # puts "Found #{num_sea_monsters} sea monsters."

    # Print the desired solution:  the number of '#' symbols left in the grid
    # after all of the sea monsters have been revealed.
    puts grid.reduce(0){ |roughness, row| roughness + row.count('#') }

    # Since we've found the solution, there's no need to keep trying any of the
    # other orientations.
    break

  end

end  # each potential layout of the actual image
