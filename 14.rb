=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 14
https://adventofcode.com/2020/day/14
=end

#initialization = IO.readlines("14_input_example.txt", chomp: true)
initialization = IO.readlines("14_input.txt", chomp: true)

# Part 1
# "Execute the initialization program.
# What is the sum of all values left in memory after it completes?"

# Method to apply a mask to a number.
def apply_mask (mask, num)

  # Make a binary number that has 0 in every place except where the mask has 1.
  mask0 = mask.tr('X', '0').to_i(2)

  # The inverse: a binary number that's all 1s except where the mask has 0.
  mask1 = mask.tr('X', '1').to_i(2)

  # Bitwise-OR the target number with the first mask, then bitwise-AND it with the second.
  return (num | mask0) & mask1

end



mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

# Using a Hash instead of an Array because indexes can be very high, and the
# array would be very sparse.
#
# "The entire 36-bit address space begins initialized to the value 0 at every
# address."
mem = Hash.new(0)



# Execute each line of the initialization program.
initialization.each do |cmd|

  # Change the mask.
  if cmd.match?(/^mask\s+\=\s+.+$/) then

    # Capture the part of the command that indicates the new value of the mask.
    cmd.match(/^mask\s+\=\s+(.+)$/)
    mask = $1

  # Set a memory address to a masked value.
  elsif cmd.match?(/^mem\[[0-9]+\]\s+=\s+[0-9]+$/) then

    # Capture the memory address and the value to be masked.
    cmd.match(/^mem\[([0-9]+)\]\s+=\s+([0-9]+)$/)
    addr = $1.to_i
    val = $2.to_i

    # Mask the value and store it in the assigned memory address.
    mem[addr] = apply_mask(mask, val)

  end

end

puts mem.values.sum



# Part 2
# "Execute the initialization program using an emulator for a version 2 decoder
# chip. What is the sum of all values left in memory after it completes?"

# There's a new example for this part of the problem.
#initialization = IO.readlines("14_input_example_2.txt", chomp: true)

# Reset the variables from part 1 so we can reuse them.
mask = '000000000000000000000000000000000000'
mem = Hash.new(0)



# Method to get a list of all the addresses indicated by a certain mask +
# address combination.
def get_addresses_from_mask (mask, addr)

  # "Values and memory addresses are both 36-bit unsigned integers."
  total_bits = 36

  # This method's return value.
  address_list = []

  # Make a binary number that has 0 in every place except where the mask has 1.
  mask0 = mask.tr('X', '0').to_i(2)

  # Bitwise-OR the address with that number.  This satisfies the first two
  # decoding rules:  "If the bitmask bit is 0, the corresponding memory address
  # bit is unchanged," and "If the bitmask bit is 1, the corresponding memory
  # address bit is overwritten with 1."
  stable_address = addr | mask0

  # Seed the address list with this number: the address we'd use if we just
  # ignored all floating bits.
  address_list.push(stable_address)



  # Walk backwards through the mask, making a new set of addresses for each
  # digit of it that is an 'X'.
  mask.chars.reverse.each_with_index do |digit, place|

    # skip non-floating bits
    next if digit != 'X'

    # Create a new array.
    new_address_list = []

    address_list.each do |addr|

      # Add an address to the list that definitely has a 1 in this place.
      # In a binary number, the value of the nth digit from the end is 2 ** n.
      new_address_list.push( addr | 2 ** place )

      # Add an address to the list that definitely has a 0 in this place.
      # To get this, we AND it with a number that is all 1s except for this place.
      new_address_list.push( addr & ( (2 ** total_bits) - (2 ** place) -1 ) )

    end

    # Replace the old address list with the new one.
    # It will keep doubling in size each time we encounter a floating bit.
    address_list = new_address_list

  end

  return address_list

end



# Execute each line of the initialization program.
initialization.each do |cmd|

  # Change the mask.
  if cmd.match?(/^mask\s+\=\s+.+$/) then

    # Capture the part of the command that indicates the new value of the mask.
    cmd.match(/^mask\s+\=\s+(.+)$/)
    mask = $1

  # Set a number of memory address to a value.
  elsif cmd.match?(/^mem\[[0-9]+\]\s+=\s+[0-9]+$/) then

    # Capture the memory address and the value to be masked.
    cmd.match(/^mem\[([0-9]+)\]\s+=\s+([0-9]+)$/)
    addr = $1.to_i
    val = $2.to_i

    # Store the value in each of the decoded memory addresses.
    get_addresses_from_mask(mask, addr).each{ |a| mem[a] = val }

  end

end



puts mem.values.sum
