=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 10
https://adventofcode.com/2020/day/10
=end

#adapters = IO.readlines("10_input_example_1.txt", chomp: true)
#adapters = IO.readlines("10_input_example_2.txt", chomp: true)
adapters = IO.readlines("10_input.txt", chomp: true)

# "Any given adapter can take an input 1, 2, or 3 jolts lower than its rating
# and still produce its rated output joltage."
difference_limit = 3

# Wrangle the input data.
adapters.map!(&:to_i).sort!

# "Treat the charging outlet near your seat as having an effective joltage rating of 0."
adapters.unshift(0)

# "...your device has a built-in joltage adapter rated for 3 jolts higher than
# the highest-rated adapter in your bag. (If your adapter list were 3, 9, and 6,
# your device's built-in adapter would be rated for 12 jolts.)"
adapters.push( adapters[-1] + difference_limit )



# Part 1
# "Find a chain that uses all of your adapters to connect the charging outlet
# to your device's built-in adapter and count the joltage differences between
# the charging outlet, the adapters, and your device. What is the number of
# 1-jolt differences multiplied by the number of 3-jolt differences?"

# We'll count the list of joltage differences using a hash.
differences = Hash.new(0)

# Step through the list of adapters, noting the difference in joltage
# ratings at each step.
adapters[0..-2].each_index do |idx|
  differences[ adapters[idx+1] - adapters[idx] ] += 1
end

#puts differences.inspect
puts differences[1] * differences[3]



# Part 2
# "What is the total number of distinct ways you can arrange the adapters to
# connect the charging outlet to your device?"
#
# I first tried to solve this problem with a recursive function, but Ruby
# raised "stack level too deep (SystemStackError)".
# So instead, I'm trying to work backwards from the end of the adapter array,
# keeping track of how many paths to the end there are at each node.

paths_to_end = Array.new( adapters.length, 0 )
paths_to_end[-1] = 1  # There isn't really one path "from the end to the end"; we're just priming the path count.

# Iterate from the second-to-last element down to the first.
(adapters.length - 2).downto(0).each do |i|

  # Iterate from the element we just chose up to the last element.
  (i + 1).upto(adapters.length - 1).each do |j|

    # if the difference between these two adapters is within the limit
    if adapters[j] - adapters[i] <= difference_limit then

      paths_to_end[i] += paths_to_end[j]

    # If the difference between these two adapters isn't within the limit, then we don't need to keep considering
    # more values of j, because we know they'll only get farther beyond the limit.
    else
      break
    end

  end

end

puts paths_to_end[0]
