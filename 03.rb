=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 3
https://adventofcode.com/2020/day/3
=end

tree_map = IO.readlines("03_input.txt", chomp: true)

# Turn the map into a two-dimensional array
tree_map.map!(&:chars)



# Part 1
# "Starting at the top-left corner of your map and following a slope of right 3 and down 1,
# how many trees would you encounter?"

class Array

  # Given two arguments, the first indicating how many spaces to move to the right in each step, and the second
  # indicating how many spaces to move down in each step, move through the map in that fashion, counting the number
  # of times you collide with a tree, and report the total.
  def find_collisions_in_slope(right = 1, down = 1)

    # check that this is a 2D array before we try anything else
    self.each_with_index do |row, idx|
      if row.class != Array then
        puts "Called find_collisions_in_slope on an array that isn't two-dimensional -- " +
             "row #{idx} is a(n) #{row.class}:\n#{row}"
        exit
      end
    end

    # If "down" is 0, we'll keep evaluating the same row eternally.
    if down == 0 then
      puts "Number of spaces to move down can't be zero or we'll never get anywhere."
      exit
    end



    # variables
    collisions = 0
    map_width = self[0].length
    xpos = 0
    ypos = 0



    # loop through the map
    while ypos < self.length do

      if self[ypos][xpos] == "#" then
        collisions += 1
      end

      xpos += right

      # If we've moved past the end of the map, wrap around.
      if xpos >= map_width then
        xpos -= map_width
      end

      ypos += down

    end

    return collisions

  end  # function find_collisions_in_slope

end  # class Array

puts tree_map.find_collisions_in_slope(3,1)



# Part 2
# "What do you get if you multiply together the number of trees encountered on each of the listed slopes?"

puts tree_map.find_collisions_in_slope(1,1) *
     tree_map.find_collisions_in_slope(3,1) *
     tree_map.find_collisions_in_slope(5,1) *
     tree_map.find_collisions_in_slope(7,1) *
     tree_map.find_collisions_in_slope(1,2)
