=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 19
https://adventofcode.com/2020/day/19
=end

require './numeric.rb'  # for String#numeric?

#input = IO.readlines("19_input_example.txt", chomp: true)
#input = IO.readlines("19_input_example_2.txt", chomp: true)
input = IO.readlines("19_input.txt", chomp: true)



# Parse the input.
divider = input.index{ |line| line.empty? }

messages = input[divider+1..]

rules = []

input[...divider].each do |line|

  m = line.match(/^([0-9]+):\s+(.+)$/)

  rules[ m[1].to_i ] = m[2].split(/\s+/).map{ |r|
    if r.numeric? then
      r.to_i
    else
      r.gsub(/"/, '')  # Strip the quotes around letters; we don't need them.
    end
  }

end



# Part 1
# "How many messages completely match rule 0?"

class Array

  # Returns a string that can be used in a regular expression to test whether a
  # string conforms to the rule.
  def get_rule (r)

    rule = self[r]

    # If this rule is just a string, then it's the end of the line; return it.
    if rule[0].class == String then

      return rule[0]

    # If this rule has an OR in it...
    # (No rules have more than one OR.
    # And every potentially recursive rule has an OR.)
    elsif ! rule.index('|').nil? then

      div = rule.index('|')  # the dividing point is the index of the |

      return '(' +
             # if this rule contains itself, which it may during part 2,
             # name this subexpression
             (rule.index(r).nil? ? '' : "?<rule#{r}>") +
             rule[...div].reduce(''){ |memo, inner_rule|
               if inner_rule == r then
                 memo + "\\g<rule#{r}>"  # reference the named subexpression
               else
                 memo + self.get_rule(inner_rule)
               end
             } +
             '|' +
             rule[(div+1)..].reduce(''){ |memo, inner_rule|
               if inner_rule == r then
                 memo + "\\g<rule#{r}>"  # reference the named subexpression
               else
                 memo + self.get_rule(inner_rule)
               end
             } +
             ')'

    # If this rule is defined by sub-rules, but doesn't have an OR...
    else

      return rule.reduce(''){ |memo, rule| memo + self.get_rule(rule) }

    end

  end  # method get_rule

end

rule_0 = rules.get_rule(0)

puts messages.reduce(0){ |memo, msg|
  if msg.match?('^' + rule_0 + '$') then
    memo + 1
  else
    memo
  end
}



# Part 2
# "After updating rules 8 and 11, how many messages completely match rule 0?"

# Modify rules 8 and 11 as instructed by the problem statement (unless we're
# working on a test input that doesn't have them).
unless rules[8].nil? then
  rules[8] = [42, '|', 42, 8]
end

unless rules[11].nil? then
  rules[11] = [42, 31, '|', 42, 11, 31]
end



# Refresh rule 0 and re-calculate the messages that follow it.
rule_0 = rules.get_rule(0)

puts messages.reduce(0){ |memo, msg|
  if msg.match?('^' + rule_0 + '$') then
    memo + 1
  else
    memo
  end
}
