=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 16
https://adventofcode.com/2020/day/16
=end

#notes = IO.readlines("16_input_example.txt", chomp: true)
#notes = IO.readlines("16_input_example_2.txt", chomp: true)
notes = IO.readlines("16_input.txt", chomp: true)

field_rules = {}
own_ticket = []
nearby_tickets = []
mode = :fields

# Process the notes.
notes.each do |rule|

  # Nothing to do on blank lines.
  next if rule.empty?

  # when to switch to the next processing mode
  if rule == "your ticket:" || rule == "nearby tickets:" then
    mode = rule.to_sym
    next
  end



  # Process a field rule.
  if mode == :fields then

    field_name, values = rule.split(/:\s+/)

    field_rules[field_name] = values
                              .split(/\s+or\s+/)
                              .map{ |rng|
                                s,e = rng.split('-').map(&:to_i)
                                Range.new(s,e)
                              }

  # Process the rule about your own ticket.
  elsif mode == :"your ticket:" then

    own_ticket = rule.split(/,/).map(&:to_i)

  # Process a rule about a nearby ticket.
  elsif mode == :"nearby tickets:" then

    nearby_tickets.push( rule.split(/,/).map(&:to_i) )

  end

end



# Part 1
# "Consider the validity of the nearby tickets you scanned.
# What is your ticket scanning error rate?"

# Scan the nearby tickets for invalid values.
error_rate = 0

nearby_tickets.each do |ticket|

  ticket.each do |value|

    if field_rules.values.flatten.find_index{ |rng|
      # If any of the ranges include this value, then the value is valid.
      # Could use ".cover?" here, but I think ".include?" is more accurate for
      # what we're doing.
      rng.include?(value)
    }.nil? then
      error_rate += value
    end

  end

end

puts error_rate




# Part 2
# "Once you work out which field is which, look for the six fields on your
# ticket that start with the word departure. What do you get if you multiply
# those six values together?"

# Start by discarding the invalid tickets.
# This is the same logic from above, just written a little differently.
nearby_tickets.reject!{ |ticket|

  ticket.map{ |value|

    if field_rules.values.flatten.find_index{ |rng|
      rng.include?(value)
    }.nil? then
      true
    else
      false
    end

  # If mapping this block onto the array of ticket fields produces any true
  # values, then the ticket is invalid.
  }.include?(true)

}



# For each field in numeric order, this array indicates which of the named
# fields it *could be*.  We start by considering all possibilites, and then
# we'll eliminate some of them.
could_be = Array.new(own_ticket.length){field_rules.keys}



# Eliminate field possibilities based on values in observed valid tickets.
(0...own_ticket.length).each do |f|

  nearby_tickets.each do |ticket|

    field_rules.each do |name, ranges|

      # If we've already figured out that this field can't be in this place,
      # move on.
      next if ! could_be[f].include?(name)

      # If ticket[f] is not in any of these ranges, then this name is not a
      # possibility for this numbered field, so delete this name from
      # could_be[f].
      if ranges.index{ |r| r.include?(ticket[f]) }.nil? then

        could_be[f].delete(name)

      end

    end  # each field rule

  end  # each nearby ticket

end  # each place in the field order



# Eliminate field possibilities based on logic.
#
# If a given field number is the only one that could possibly belong to a
# certain name, then we can discard all possibilities for it other than that
# name.
#
# We repeatedly iterate over the array, discarding possibilities in this manner,
# until only one possibility remains for each field.
#
# (If we get into a situation where there are multiple fields with the same set
# of possibilities, this loop will never stop, but that would also imply that
# the problem is unsolvable.)
while ! could_be.index{ |item| item.length > 1 }.nil? do

  field_rules.keys.each do |name|

    if could_be.select{ |possibilities| possibilities.include?(name) }.count == 1 then

      i = could_be.index{ |possibilities| possibilities.include?(name) }
      could_be[i] = [name]  # still need to store it as an array even though it only has one element

    end

  end

end



# Figure out which fields are the departure fields.
departure_fields = []
could_be.each_with_index do |name, i|
  if name[0].index('departure') === 0
    departure_fields.push(i)
  end
end



# Multiply the departure fields from our own ticket together, and display the
# result.
puts departure_fields.reduce(1){ |memo,i| memo * own_ticket[i] }
