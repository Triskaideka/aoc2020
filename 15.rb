=begin
Advent of Code 2020
https://adventofcode.com/2020

Triskaideka
https://triskaideka.net/

Day 15
https://adventofcode.com/2020/day/15
=end

#starting_numbers = [0,3,6]
starting_numbers = [13,0,10,12,1,5,8]

# Part 1
# Given the starting numbers and the rules of this game,
# "what will be the 2020th number spoken?"
#
# Part 2
# "Given your starting numbers, what will be the 30000000th number spoken?"
#
# This solution takes a few seconds to run, so it may not be the intended
# algorithm.  On the other hand, it *only* takes a few seconds to run.

def play_number_game (s, target)

  # We alter the first argument in this method, which is a problem if we're
  # going to call the method more than once, which we are.  So let's start by
  # making a copy of it
  starting = s.clone

  # Separate out the last number of the starting list;
  # it will be the first one we "speak" in this method.
  last_spoken = starting.pop

  # By using a hash for storage, we can keep the transcript noticeably smaller
  # than the size of "target".
  transcript = Hash.new(nil)

  starting.each_with_index do |n, t|
    transcript[n] = t + 1  # the transcript starts at 1, not 0
  end

  ((starting.length + 1)...target).each do |t|

    # Set the next number according to the rules of the game.
    if transcript[last_spoken].nil? then
      next_spoken = 0
    else
      next_spoken = t - transcript[last_spoken]
    end

    # Move on to the next number.
    transcript[last_spoken] = t
    last_spoken = next_spoken

  end

  return last_spoken

end  # method play_number_game



puts play_number_game(starting_numbers, 2020)
puts play_number_game(starting_numbers, 30000000)
